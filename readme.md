# Accélérations de requêtes de voisinage

Ce TP aborde la géométrie algorithmique, et plus particulièrement les structures
accélératrices pour les requêtes géométriques. Nous commencerons par étudier la
requête de plus proche voisins en 2D : étant donné un ensemble de points et une
requête (un autre point), déterminer le point de l'ensemble le plus proche de la
requête. Le but est surtout ici de répondre à ce problème lorsque l'ensemble de
points ne varie pas, mais que beaucoup de requêtes sont réalisées. Dans ce cas
il est alors possible de commencer par organiser l'ensemble de points pour
pouvoir par la suite réaliser les requêtes plus efficacement.

<!--
IGNORER

## Conditions de travail

Ce TP est individuel, à rendre en fin de séance. Votre travail est à rendre sous
la forme d'une archive `.tar.gz` ou `.zip` sur
[TOMUSS](https://tomuss.univ-lyon1.fr). Durant la durée du TP, vous êtes
autorisé·es à consulter vos anciens TP, à naviguer sur internet pour chercher de
la documentation. Vous pouvez également communiquer entre vous, même si le
travail rendu doit être personnel et individuel.

FIN IGNORER
-->

## Base de code

Une base de code en C++ vous est fournie pour ce TP. Elle fournit un ensemble
de tests suivant la progression du TP pour tester au fur et à mesure la mise en
place des fonctionnalités demandées. Par défaut aucun test n'est actif, à vous
de les activer lorsque vous avez mis en place la fonctionnalité correspondante.
Ces tests génèrent des images dans le dossier d'exécution au format `svg`.

Ce code s'intègre dans la base de code 
[gkit3](https://forge.univ-lyon1.fr/JEAN-CLAUDE.IEHL/gkit3) déjà utilisée lors
des TP précédents. Pour l'intégrer dans cette base, suivez les étapes suivantes
:

1. naviguez dans le dossier `projets` de gkit3
1. clonez ce dépôt dans ce dossier
2. modifiez le fichier `premake4.lua` à la racine de gkit3 en y ajoutant

```lua
project("bvh")
  language "C++"
  kind "ConsoleApp"
  targetdir "bin"
  buildoptions ( "-std=c++17" )
  files ( gkit_files )
  files { "projets/bvh-etu/src/*.cpp" }
```

## Plus proche voisin naif

Pour se donner une base et vérifier l'algorithme plus élaboré qui viendra
ensuite, mettez en place une fonction prenant en paramètre un tableau de
points et une requête et retournant l'indice du point le plus proche dans le
tableau de points. Pas d'algorithme compliqué ici, un simple parcours linéaire
du tableau pour trouver le plus proche suffit.

#### Contraintes

Cette fonction est à implémenter dans le fichier `naive_nearest.cpp` en
respectant le prototype proposé. La structure `Point` utilisée pour ce code est
celle fournie par gkit3, n'hésitez pas à [consulter la documentation](
https://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/group__math.html)
en particulier la fonction `distance` permettant de calculer la distance entre
deux points.

#### Test à activer 

Le test est à activer en décommentant la ligne
```cpp
#define NN_TEST
```
Il utilise par défaut un ensemble de 200 points, et réalise 8000 requêtes de
voisinages. Si votre fonction est correcte, vous devriez obtenir joli feu
d'artifice :

<center>
<img id="nn-test" src="img/nn.svg", width=500px"></img>
</center>

Le test dessine pour chaque requête un segment la reliant à son plus proche voisin
parmi les 200 points, et colore les requêtes et les segments de la couleur de
leur point le plus proche.

## Hiérarchie de boîtes englobantes

La base de code vous fournit une structure de boîte englobante alignée sur les
axes dans les fichiers`box.h` et `box.cpp`. Ces boîtes ont pour but de stocker
les bornes d'un ensemble de points : le minimum et le maximum des coordonnées
des points qu'elle englobe sur chaque axe. Les boîtes **ne stockent pas les
points**, seulement les bornes, sous la forme de deux points, `min` et `max`.

<center>
<img id="box-build-test" src="img/box_build.svg", width=500px"></img>
</center>

Pour construire une boîte, vous pouvez utiliser la méthode `push` pour lui
spécifier des points à englober :

```cpp
Box b ;
b.push(Point(0,0,0)) ;
b.push(Point(1,1,1)) ;
//...
```

Le but des boîtes est de pouvoir éliminer tous les points qu'elle englobe d'un
coup : la distance entre un point et une requête est plus grande que la distance
entre la boîte qui l'englobe et la requête. Ainsi si la boîte est trop loin de
la requête, alors tous les points qu'elle contient également. Pour pouvoir
utiliser cette technique, il est donc nécessaire de pouvoir calculer la distance
entre une requête et une boîte. La méthode `nearest` permet de calculer cette
distance en calculant le point de la boîte le plus proche d'une requête. La
distance entre ce point et la requête est donc la distance entre la boîte et la
requête.

<center>
<img id="box-nearest-test" src="img/box_nearest.svg", width=500px"></img>
</center>

```cpp
Box b ;
//...

Point query(0,0,0) ;
distance(box.nearest(query), query) ;
```

Muni de boîtes englobantes, il faut maintenant se préoccuper de la méthode pour
regrouper les points dans ces boîtes. Les hiérarchies de volumes englobant
généralisent le concept d'arbre binaire de recherche à des données de plus
grande dimension. Dans un arbre binaire de recherche, à chaque nœud, on choisit
une valeur et on répartit d'un côté les valeurs plus petites, et de l'autre les
plus grandes. Ici, nous allons également construire un arbre binaire. Chaque
nœud contient une boîte qui englobe une partie des points. Pour construire les
enfants d'un nœud de l'arbre, il faut choisir **un axe et une valeur**. Les
points englobés par la boîte sont ensuite répartis entre les deux enfants selon 
que leur coordonnée sur l'axe est plus grande ou plus petite que la valeur.

### Premier noeud de l'arbre binaire

Notre hiérarchie de volumes englobants est donc un **arbre binaire** : chaque
nœud de l'arbre a deux enfants. De plus chaque nœud stocke la boîte englobante
des points qu'il englobe. Pour l'instant nous cherchons juste à créer un nœud,
et pas encore d'enfants ou de hiérarchie.

#### Contraintes

Travaillez dans les fichiers `tree.h` et `tree.cpp`. Vous y développerez une
classe `Node` pour représenter les nœuds de l'arbre. Pour construire un nœud, il
faudra fournir un `vector` de points ainsi que deux indices `debut` et `fin`
définissant la zone du tableau que le nœud doit englober. Lors de la
construction, le nœud calculera la boîte englobante de ses points. Votre
programme devra permettre la création d'un nœud en utilisant la syntaxe

```cpp
std::vector<Point> points ;

points.push_back(Point(0,0,0)) ;
points.push_back(Point(1,1,1)) ;
//...

Node n(points, 0, points.size()) ;
```
Pour que les tests fonctionnent, il devra également être possible de récupérer la 
boîte englobante d'un nœud via une méthode `box`. Cette méthode doit être
étiquetée avec un `const` pour spécifier qu'elle ne modifie pas le nœud.

```cpp
//...
Box b = n.box() ;
```
Le nœud n'a pas besoin de stocker les points qu'il englobe. Par contre, il
stockera les indices `debut` et `fin` de la plage du tableau qu'il englobe.

#### Test à activer

Le test s'active en décommentant la ligne

```cpp
#define ROOT_NODE_TEST
```

Il génère un ensemble de points dans deux disques, puis construit un nœud sur
la moitié des points, dans le premier disque. Le test devrait donc afficher une
boîte englobant les points de l'un des deux disques :

<center>
<img id="box-nearest-test" src="img/root_node.svg", width=500px"></img>
</center>


### Répartition des points de part et d'autre d'une valeur sur un axe

Pour créer les enfants d'un nœud, il faut donc choisir un axe et une valeur pour
répartir ses points entre les deux enfants. Nous allons écrire une fonction
permettant de réorganiser une portion d'un tableau de points pour regrouper les
points de chaque enfant. Le prototype de cette fonction est donnée dans
`tree.h`. Votre travail consiste à en donner une implémentation.

#### Contraintes

Vous devez respecter le prototype de la fonction `partition` proposée dans
`tree.h`. Cette fonction prend en paramètre un axe (0 pour x, 1 pour y), et
une valeur. Elle réordonne une plage d'un tableau pour mettre au début les
points de la plage ayant une coordonnée sur l'axe plus petite que la valeur, et
à la fin les points ayant une coordonnée sur l'axe plus grande que la valeur. Le
tableau est également fourni en paramètre à la fonction, ainsi que le début et
la fin de la plage. Par exemple, sur le tableau de points

```
[(3,8), (1,4), (2, 7), (2, 2), (5, 4), (6, 3), (4, 5), (3, 7)]
```

en réordonnant les points sur la plage débutant en 2 (inclus) et finissant en 7
(exclus), selon l'axe 0 avec la valeur 4, le tableau après l'appel pourrait
contenir

```
[(3,8), (1,4) | (2, 7), (4, 5), (2, 2) | (6, 3), (5, 4) | (3, 7)]
              |                        |                |
  ignoré      |          x ≤ 4         |      x > 4     | ignoré
```

Ici, les points `(3,8)`, `(1,4)` et `(3,7)` n'ont pas bougé car ils sont en
dehors de la plage modifiée, et les autres points ont été réordonnés. Ici, le
point ayant un x égal à la valeur a été mis au début. La fonction retourne
l'indice du premier point ayant une coordonnée sur l'axe plus grande que la
valeur. Dans l'exemple ci-dessus, elle retourne 5 qui est l'indice de `(6,3)`.

Pour implémenter cette fonction, le plus simple consiste à créer deux `vector`
intermédiaires : un pour points du début, et un pour les points de la fin. 
Vous pouvez alors parcourir linéairement la plage du tableau à modifier, et
recopier les points dans le bon `vector` selon que leur coordonnée est plus
petite ou plus grande que la valeur. Une fois les deux `vector` remplis, dans
uns seconde passe, vous pouvez recopier les points du `vector` des plus petits
dans le `vector` initial en commençant au début de la plage, puis lorsque c'est 
bon, continuer en recopiant les plus grands à la suite, jusqu'à avoir totalement
réécrit la plage. La valeur à retourner est la position dans le tableau où vous
avez commencé à recopier les points plus grands.

Il est possible de coder cette fonction en réalisant un pivot sans créer de
tableau supplémentaire, comme vu en cours d'algorithmique pour le Quick Sort.
Si vous avez fini le TP en avance, vous pouvez tenter cette version. Cet
fonctionnalité est également disponible dans la bibliothèque standard via la
fonction `std::partition`.

#### Test à activer

Pour tester votre découpe, décommentez la ligne

```cpp
#define POINT_SPLIT_TEST
```

Ce test crée un ensemble de points, puis réordonne une partie du tableau en
utilisant votre fonction `partition` selon l'axe x avec la valeur 0.5. Il
récupère la valeur de retour de votre fonction, puis dessine :
* les points ignorés en jaune
* les points de la zone traitée plus petits que la valeur de retour en vert
* les points de la zone traitée plus grands que la valeur de retour en rouge

La valeur 0.5 de partition est indiquée par une ligne verticale. Si tout se
passe bien, vous devriez avoir les points verts à gauche de la ligne et les
rouges à droite. Les points jaune ayant été ignorés, il devrait y en avoir des
deux côtés.

<center>
<img id="point-split-test" src="img/point_split.svg", width=500px"></img>
</center>

### Découpe d'un nœud

Pour découper un nœud, il faut choisir l'axe, la valeur, et utiliser votre
`partition` pour réordonner les points. Les enfants peuvent ensuite être créés
sur leurs portions de tableau respectives. Pour choisir l'axe, nous utiliserons
la méthode suivante :

1. calculer la largeur et la hauteur de la boîte
1. choisir l'axe le plus long comme axe de découpe
1. choisir comme valeur de découpe le milieu de cet axe

#### Contraintes

Vous ajouterez une méthode `split` à votre classe nœud. En appelant cette
méthode, un nœud sans enfant déterminera l'axe et la valeur de découpe,
réordonnera la portion du tableau de points contenant les points du nœud, et
créera les enfants sur les deux portions créées. Pour pouvoir réaliser cette
action, votre classe `Node` doit également stocker les indices `debut` et 
`fin` correspondant à la portion du tableau contenant ses points. Votre méthode
prendra en paramètre le tableau de points sur lequel l'arbre est construit.

Ainsi, il sera possible de découper un nœud pour former ses enfants via la
syntaxe

```cpp
n.split(points) ;
```

et vous rendrez accessible la plage du tableau d'un nœud via

```cpp
int b = n.begin() ; //debut de la plage du tableau (inclus)
int e = n.end() ; //fin de la plage du tableau (exclus)
```

#### Test à activer

Pour vérifier la découpe, décommentez la ligne

```cpp
#define NODE_SPLIT_TEST
```

Ce test construit un ensemble de points en trois parties : deux petits disques
et un large rectangle. Il crée ensuite un noeud uniquement sur le rectangle,
puis demande la découpe de ce noeud. La forme du rectangle devrait provoquer une
découpe selon l'axe des x. En sortie, le test affiche l'ensemble des points. Les
points qui sont dans les plages des enfants sont représentés avec une couleur
par enfant, et devraient être encadrés par les boîtes des enfants. Les points
qui ne sont pas touchés par la modification devraient apparaître multicolores en
dehors des boîtes :

<center>
<img id="node-split-test" src="img/node_split.svg", width=500px"></img>
</center>

### Création d'un arbre complet

Une fois la découpe fonctionnelle, il ne reste plus qu'à découper récursivement
les nœuds pour former un arbre. La racine est initialisée pour référencer
l'ensemble des points, puis tant que les nœuds référencent plus d'un certain
nombre de points, ils sont découpés.

Ajoutez une méthode `rec_split `à votre classe `Node` pour réaliser cette découpe 
récursive.

#### Contraintes

Votre méthode prendra en paramètre le tableau de points sur lequel l'arbre est
construit, ainsi que le nombre de points en dessous duquel un nœud n'est plus
découpé. On peut alors créer un arbre complet avec la syntaxe

```cpp
std::vector<Point> points ;

points.push_back(Point({0.5, 0.5})) ;
//...

Node tree(points, 0, points.size()) ;
tree.rec_split(points, 10) ;
```

#### Test à activer

Décommentez la ligne 
```cpp
#define TREE_CREATE_TEST
```

Ce test visualise l'arbre ainsi créé. Il initialise un arbre sur une centaine de
points, appelle la méthode `rec_split` avec par un seuil à 10 points, et enfin
affiche les boîtes englobantes de tous les nœuds de l'arbre créé, ainsi que les
points référencés par les feuilles de l'arbre d'une couleur assortie à celle de
leur feuille. Vous devriez ainsi obtenir une image comme ci-dessous.

<center>
<img id="tree-create-test" src="img/tree_create.svg", width=500px"></img>
</center>

### Plus proche voisin dans un arbre

Vous pouvez désormais exploiter votre arbre pour accélérer vos requêtes de
voisinage. L'idée est de commencer à la racine de l'arbre, et de maintenir un
point candidat pour être le plus proche de la requête, ainsi que sa distance à
la requête. À chaque nœud, si la distance entre la boîte de ses enfants et la
requête est plus petite que la distance du point candidat, l'enfant est visité,
ce qui mettra à jour si besoin le point candidat et sa distance. Pour plus
d'efficacité, il faut visiter d'abord l'enfant le plus proche de la requête.

#### Contraintes

Ajoutez une méthode `nearest` permettant de réaliser la requête de point le plus
proche avec la syntaxe

```cpp
int index = n.nearest(points, query) ;
```
Cette fonction renvoie l'indice dans le tableau points du point le plus proche
de la requête. Pour implémenter cette fonction, il pourra être utile de définit
une méthode auxiliaire qui prend en plus deux paramètres : l'indice du point le
plus proche trouvé jusqu'à présent, et la distance entre ce point et la requête.
L'indice du point le plus proche peut être initialisé avec 0 (ou n'importe quel
autre indice valide de point), et la distance entre le point 0 et la requête.
L'idée de cette fonction est de dire :

1. Si le nœud est une feuille, tester linéairement sur ses points s'il y en a
   un plus proche que celui qu'on a
2. Sinon calculer la distance entre la requête et les boîtes des deux enfants
3. Si l'enfant le plus proche est à une distance plus petite que la distance
   actuelle, l'explorer récursivement
4. Si l'entant le plus éloigné est à une distance plus petite que la distance
   actuelle, l'explorer récursivement
5. Retourner l'indice du point le plus proche

Lors des explorations récursives, l'appel est réalisé avec en paramètre l'indice
du point actuellement le plus proche. Si l'appel retourne un indice différent,
c'est qu'un point encore plus proche a été trouvé, il faut alors mettre à jour
le point le plus proche et la distance correspondante.

#### Test à activer

Assurez-vous ensuite de décommenter les lignes
```cpp
#define NN_TEST
#define TREE_NEAREST_TEST
```

Ce test se rajoute à celui de votre algorithme naïf pour tester votre nouvel
algorithme, et il vérifie que les résultats des deux algorithmes sont
identiques. [L'image produite ne devrait pas changer](#nn-test). Le test mesure
également les temps d'exécution pour vérifier que cette méthode est plus rapide
que la naïve.

## Pour aller plus loin

Le principe des hiérarchies de volumes englobant s'applique également sur
d'autres types de primitives géométriques : segments, triangles, sphères, ... et
également sur d'autres types de requêtes, en particulier pour l'intersection
rayon / triangle que vous avez étudiée dans les TP précédents. Vous pouvez donc
aller plus loin que l'exercice de ce TP en :

### Passage en 3D

Normalement, dans l'implémentation, le seul endroit où la dimension est
importante est au niveau du découpage d'un nœud où il faut également envisager
de découper selon le troisième axe. Vous pouvez donc vous assurer que vous y
arrivez, et rajouter un test pour générer des points en 3D et vous assurer que
l'algorithme naïf et vos arbres fournissent toujours le même résultat.

### Hiérarchie sur des triangles

Les triangles peuvent être gérés en ajoutant plusieurs fonctionnalités
* pour qu'une boîte englobe un triangle, il suffit qu'elle englobe ses trois
  sommets.
* lors de la répartition des primitives de part et d'autre d'un axe, un tiangle
  n'est pas nécessairement d'un côté ou de l'autre d'une valeur. Une technique
  classique consiste à répartir les triangles en fonction de la position de leur
  barycentre, ou du barycentre de leur boîte englobante.

### Intersection rayon boîte

Pour qu'un rayon intersecte un triangle, il doit entrer dans les boîtes qui
l'englobent, et ainsi intersecter ces boîtes également. Pour pouvoir exploiter
votre hiérarchie pour le lancer de rayons, il faut alors rajouter une méthode
d'intersection rayon / boîte.

### Exploiter la hiérarchie pour le lancer de rayons

Le parcours de l'arbre pour le lancer de rayons est similaire à celui de la
recherche de plus proche voisin. L'élément clé consiste à stocker la distance
entre la source du rayon et l'intersection la plus proche trouvée jusqu'à
présent. Même si l'intersection entre une boîte et un rayon existe, si elle est
plus loin que cette distance, le contenu de la boîte peut être ignoré. D'autre
part, au niveau d'un nœud, s'il faut visiter les deux enfants, il est plus
intéressant de commencer par visiter celui ayant l'intersection la plus proche
de la source avec le rayon.
